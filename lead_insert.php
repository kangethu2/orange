<?php
include '../db_connections/config.php';

// prepare and bind
$stmt = $conn->prepare("INSERT INTO website_leads (name, town, mobile, email, product, value) VALUES (?,?,?,?,?,?)");
$stmt->bind_param("ssdsss", $name, $town, $mobile, $email, $product, $value);

// set parameters and execute
$name = $_POST["name"];
$town = $_POST["town"];
$mobile = $_POST["mobile_no"];
$email = $_POST["email_address"];
$product = $_POST["product"];
$value = $_POST["value"];
// $first_name = "first_name";
// $last_name = "last_name";
// $mobile = "02222";
// $email = "email1";
// $product = "product1";
// $location = "location3";
$stmt->execute();

echo "New records created successfully";

$stmt->close();
$conn->close();
?>
